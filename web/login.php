
<!DOCTYPE html>
<html lang="zxx">

<head>
    <title>barber website</title>
    <!-- Meta tag Keywords -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8" />
    <meta name="keywords" content="Infinitude Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
    <script>
        addEventListener("load", function() {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }

    </script>
    <!-- //Meta tag Keywords -->
    <!-- Custom-Files -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <!-- Bootstrap-Core-CSS -->
    <link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
    <!-- Style-CSS -->
    <!-- font-awesome-icons -->
    <link href="css/font-awesome.css" rel="stylesheet">
    <!-- //font-awesome-icons -->

</head>
<body>
    <section class="banner_bottom py-5" id="appointment">
  
        <div class="container py-md-5">
            <div class="row inner_sec_info">

                </div>
                <div class="col-lg-5 banner_bottom_left">

                    <div class="login p-md-5 p-4 mx-auto bg-white mw-100">
                        <h4>
                            Login</h4>
                        <form action="connect.php" method="post">
                            <div class="form-group">
                                <label>Email</label>

                                <input type="text" name="email" class="form-control" id="validationDefault01" placeholder="" required="">
                            </div>
                            <div class="form-group mb-4">
                                <label class="mb-2">Password</label>
                                <input type="password" name="password" class="form-control" id="password1" placeholder="" required="">
                            </div>

                            <button type="submit" class="btn more black submit mb-4" name="login"> Login</button><br><br>
                            <a class="link-hny" href="logout.html">Do not have an account?</a>
                        </form>
                    </div>

                </div>
                <div class="col-lg-7 banner_bottom_grid help pl-lg-5">
                    <img src="images/ab2.jpg" alt=" " class="img-fluid mb-4">
                    <h4><a class="link-hny" href="contact.html">Have a hair cut?</a></h4>
                    <p class="mt-3">Come and join us </p>

                </div>
            </div>

        </div>
    </section>
</body>
</html>