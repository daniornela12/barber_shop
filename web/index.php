
<!DOCTYPE html>
<html lang="zxx">

<head>
    <title>barber website</title>
    <!-- Meta tag Keywords -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8" />
    <meta name="keywords" content="Infinitude Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
    <script>
        addEventListener("load", function() {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }

    </script>
    <!-- //Meta tag Keywords -->
    <!-- Custom-Files -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <!-- Bootstrap-Core-CSS -->
    <link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
    <!-- Style-CSS -->
    <!-- font-awesome-icons -->
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/calendar.css" type="text/css" rel="stylesheet" />
    <!-- //font-awesome-icons -->

</head>

<body>
    <!-- home -->
    <div id="home">
        <!--/top-nav -->
        <div class="top_w3pvt_main container">
            <!--/header -->
            <header class="nav_w3pvt text-center ">
                <!-- nav -->
                <nav class="wthree-w3ls">
                    <div id="logo">
                        <h1> <a class="navbar-brand px-0 mx-0" href="index.html">RAIE CREATION
                            </a>
                        </h1>
                    </div>

                    <label for="drop" class="toggle">Menu</label>
                    <input type="checkbox" id="drop" />
                    <ul class="menu mr-auto">
                        <li class="active"><a href="index.html">Home</a></li>
                        <li><a href="about.html">About</a></li>
                        <li>
                            <!-- First Tier Drop Down -->
                            <label for="drop-2" class="toggle toggle-2">Pages <span class="fa fa-angle-down" aria-hidden="true"></span> </label>
                            <a href="#">Pages  <span class="fa fa-angle-down" aria-hidden="true"></span></a>
                            <input type="checkbox" id="drop-2" />
                         
                        </li>
                        <li><a href="contact.html">Contact</a></li>

                        <li class="social-icons ml-lg-3"><a href="#" class="p-0 social-icon"><span class="fa fa-facebook-official" aria-hidden="true"></span>
                                <div class="tooltip">Facebook</div>
                            </a> </li>
                        <li class="social-icons"><a href="#" class="p-0 social-icon"><span class="fa fa-twitter" aria-hidden="true"></span>
                                <div class="tooltip">Twitter</div>
                            </a> </li>
                        <li class="social-icons"><a href="#" class="p-0 social-icon"><span class="fa fa-instagram" aria-hidden="true"></span>
                                <div class="tooltip">Instagram</div>
                            </a> </li>
                    </ul>
                </nav>
                <!-- //nav -->
            </header>
            <!--//header -->
        </div>
        <!-- //top-nav -->
        <!-- banner slider -->
        <div id="homepage-slider" class="st-slider">
            <input type="radio" class="cs_anchor radio" name="slider" id="play1" checked="" />
            <input type="radio" class="cs_anchor radio" name="slider" id="slide1" />
            <input type="radio" class="cs_anchor radio" name="slider" id="slide2" />
            <input type="radio" class="cs_anchor radio" name="slider" id="slide3" />
            <div class="images">
                <div class="images-inner">
                    <div class="image-slide">
                        <div class="banner-w3pvt-1">
                            <div class="overlay-w3ls">

                            </div>

                        </div>
                    </div>
                    <div class="image-slide">
                        <div class="banner-w3pvt-2">
                            <div class="overlay-w3ls">

                            </div>
                        </div>
                    </div>
                    <div class="image-slide">
                        <div class="banner-w3pvt-3">
                            <div class="overlay-w3ls">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="labels">
                <div class="fake-radio">
                    <label for="slide1" class="radio-btn"></label>
                    <label for="slide2" class="radio-btn"></label>
                    <label for="slide3" class="radio-btn"></label>
                </div>
            </div>
            <!-- banner-hny-info -->
            <div class="banner-hny-info">
                <h3>Come and have your hair cut
                    <br>Take an appointment</h3>
                <div class="top-buttons mx-auto text-center mt-md-5 mt-3">
                    <a href="contact.html" class="btn">Contact Us</a>
                </div>
            </div>
            <!-- //banner-hny-info -->
        </div>
        <!-- //banner slider -->
    </div>
    <!-- //banner -->

    <!-- //home -->

    <!-- about -->
    <section class="about py-5">
        <div class="container p-md-5">
            <div class="about-hny-info text-left px-md-5">
                <h3 class="tittle-w3ls mb-3"><span class="pink">We</span> Create model and looks to your hairs</h3>
                <p class="sub-tittle mt-3 mb-4">In less than a minute,just look a date and be happy</p>
            </div>
        </div>
    </section>
    <!--//services-->
    <!-- /projects -->
    <section class="projects py-5" id="gallery">
        <div class="container py-md-5">
            <h3 class="tittle-w3ls text-left mb-5"><span class="pink">Hair </span> Style</h3>
            <form action="images.php" method = "POST">
            <div class="row news-grids mt-md-5 mt-4 text-center">
                <div class="col-md-4 gal-img">
                    <a href="#gal1"><img src="images/barber.jpg" class="img-fluid"></a>
                    <div class="gal-info">
                        <h5>Curling<span class="decription">hair</span></h5>
                    </div>
                </div>
                <div class="col-md-4 gal-img">
                    <a href="#gal2"><img src="images/barber.jpg"  class="img-fluid"></a>
                    <div class="gal-info">
                        <h5>Short<span class="decription">cut</span></h5>
                    </div>
                </div>
                <div class="col-md-4 gal-img">
                    <a href="#gal3"><img src="images/barber.jpg" class="img-fluid"></a>
                    <div class="gal-info">
                        <h5>Twists style<span class="decription">Hair</span></h5>
                    </div>
                </div>
                <div class="col-md-4 gal-img">
                    <a href="#gal4"><img src="images/barber.jpg"  class="img-fluid"></a>
                    <div class="gal-info">
                        <h5>Brushing<span class="decription">Hair</span></h5>
                    </div>
                </div>

                <div class="col-md-4 gal-img">
                    <a href="#gal5"><img src="images/barber.jpg"  class="img-fluid"></a>
                    <div class="gal-info">
                        <h5>Stylish<span class="decription">Hair</span></h5>
                    </div>
                </div>
                <div class="col-md-4 gal-img">
                    <a href="#gal6"><img src="images/barber.jpg"  class="img-fluid"></a>
                    <div class="gal-info">
                        <h5>Crawl<span class="decription">Hair</span></h5>
                    </div>
                </div>
                <div class="col-md-4 gal-img">
                    <a href="#gal7"><img src="images/barber.jpg" alt="w3pvt" class="img-fluid"></a>
                    <div class="gal-info">
                        <h5>Shower<span class="decription">Hair</span></h5>
                    </div>
                </div>
                <div class="col-md-4 gal-img">
                    <a href="#gal8"><img src="images/barber.jpg" class="img-fluid"></a>
                    <div class="gal-info">
                        <h5>Creadle<span class="decription">Hair</span></h5>
                    </div>
                </div>
                <div class="col-md-4 gal-img">
                    <a href="#gal9"><img src="images/barber.jpg"  class="img-fluid"></a>
                    <div class="gal-info">
                        <h5>Tenders<span class="decription">Hair</span></h5>
                    </div>
                </div>

            </div>
            <!-- popup-->
           
            <!-- //popup -->
    </form>
        </div>
    </section>
    <!-- //projects -->
    <!-- /blogs -->
   
    <!-- //blogs -->
    <!--/mid-->
    <section class="banner_bottom py-5" id="appointment">
        <div class="container py-md-5">
            <div class="row inner_sec_info">


                <div class="col-lg-12 banner_bottom_left">

                    <div class="login p-md-1 p-1 mx-auto bg-white mw-100">
                        <h4>
                            Take an appointment</h4> <br><br>
                        <form action="#" method="post">
                            <!-- inserer rendez vous ici -->
                            <?php
include 'calendar.php';
 
$calendar = new Calendar();
 
echo $calendar->show();
?>
<br><br>
                             
                            <button type="submit" class="btn more black submit mb-8" style="marging-left:50%">Appointment</button>

                        </form>

                    </div>

                </div>
            </div>

        </div>
    </section>


    <!-- /news-letter -->
    <section class="news-letter-w3pvt py-5">
        <div class="container contact-form mx-auto text-left">
            <h3 class="title-w3ls two text-left mb-3">Newsletter </h3>
            <form method="post" action="#" class="w3ls-frm">
                <div class="row subscribe-sec">
                    <p class="news-para col-lg-3">Start working together?</p>
                    <div class="col-lg-6 con-gd">
                        <input type="email" class="form-control" id="email" placeholder="Your Email here..." name="email" required>

                    </div>
                    <div class="col-lg-3 con-gd">
                        <button type="submit" class="btn submit">Subscribe</button>
                    </div>

                </div>

            </form>
        </div>
    </section>
    <!-- //news-letter -->

    <!-- footer -->
    <footer class="py-lg-5 py-4">
        <div class="container py-sm-3">
            <div class="row footer-grids">
                <div class="col-lg-4 mt-4">

                    <h2> <a class="navbar-brand px-0 mx-0 mb-4" href="index.html">Infinitude
                        </a>
                    </h2>
                    <div class="icon-social mt-4">
                        <a href="#" class="button-footr">
                            <span class="fa mx-2 fa-facebook"></span>
                        </a>
                        <a href="#" class="button-footr">
                            <span class="fa mx-2 fa-twitter"></span>
                        </a>
                        <a href="#" class="button-footr">
                            <span class="fa mx-2 fa-dribbble"></span>
                        </a>
                        <a href="#" class="button-footr">
                            <span class="fa mx-2 fa-pinterest"></span>
                        </a>
                        <a href="#" class="button-footr">
                            <span class="fa mx-2 fa-google-plus"></span>
                        </a>

                    </div>
                </div>
               
        </div>
    </footer>
    <!-- //footer -->
    <!-- copyright -->
    <div class="copy_right p-3 d-flex">
        <div class="container">
            <div class="row d-flex">
                <div class="col-lg-9 copy_w3pvt">
                    <p class="text-lg-left text-center">© 2019 RAIE CREATION All rights reserved | Design by

                </div>
                <!-- move top -->
                <div class="col-lg-3 move-top text-lg-right text-center">
                    <a href="#home" class="move-top">
                        <span class="fa fa-angle-double-up mt-3" aria-hidden="true"></span>
                    </a>
                </div>
                <!-- move top -->
            </div>
        </div>

    </div>
    <!-- //copyright -->
</body>

</html>
<script src="js\jquery-3.4.1.min.js">
</script>
<script>
    function todo(id){
        $("#hour").empty();
        texte="Choose an hour : ";
        hour="<select id='heure'>"+"<?php for($i=8;$i<18;$i++){ echo "<option value='".$i."'>".$i."</option>" ;}?>"+"</select>H";
        minute="<select id='min' style='margin-left: 20px;'>"+"<?php for($i=0;$i<60;$i++){ echo "<option value='".$i."'>".$i."</option>" ;}?>"+"</select>MIN";
        date="<span style='margin-left: 20px;'> for <span style='margin-left: 20px;'>"+id+"</span></span>"
        texte+=hour+minute+date;
        $("#hour").append(texte);
    }
</script>
