-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Ven 24 Janvier 2020 à 21:59
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `barber_shop`
--

-- --------------------------------------------------------

--
-- Structure de la table `hair_style`
--

CREATE TABLE IF NOT EXISTS `hair_style` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `image` varchar(1500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Contenu de la table `hair_style`
--

INSERT INTO `hair_style` (`id`, `name`, `image`) VALUES
(1, 'coiffure carrée', 'https://i.pinimg.com/736x/3a/23/b7/3a23b72b3532d2a9ff645f4392e810ed.jpg'),
(2, 'hair_short', 'http://coupedecheveuxhomme.org/wp-content/uploads/2017/12/jose.crespo_-wavy-hairstyle-for-men-mid-fade-haircut.jpg'),
(3, 'curling', 'https://www.sarl-rodolphe.fr/wp-content/uploads/2018/04/text-homme.jpg'),
(4, 'shorter_hair', 'https://mk0jamaisvulgai5hw11.kinstacdn.com/wp-content/uploads/2015/05/undercut-coiffure-mode.jpg'),
(5, 'line_hair', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSnbJIr8UexSwMt_KVIHCXw0vYojjHQXgoSrE9I0PdZ70cNX2z7&s'),
(6, 'marker', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRnzQa3z0OUSuIxn8vS-9gz9FjKPZyJeCRbtRga81ms7U-GTgepgg&s'),
(7, 'hair_cutter', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQXezcboXDkzyD683Aufa1ySimt2SifJhMTYqif5_SCSKaoEU6cWA&s'),
(8, 'hair_brush', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTXHg2urbQgw147r1g6tRRDELZvczdSGzwC7BCneiZTLDdSiYVH&s'),
(9, 'blurd', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTXHg2urbQgw147r1g6tRRDELZvczdSGzwC7BCneiZTLDdSiYVH&s');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`id`, `first_name`, `last_name`, `email`, `password`) VALUES
(1, 'admin', 'admin', 'admin@gmail.com', 'admin'),
(6, 'Dani', 'Ornela', 'daniornela12@gmail.com', 'dani'),
(7, 'Danielle Ornella', 'TCHOUOGOUONG', 'simodanielleornella@yahoo.fr', 'dani');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
